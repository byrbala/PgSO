#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <signal.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>

#define NOMECINEMA "Vedo tutto buio"
#define NFILE 30
#define NPOLTRONE 20
#define FILENAME "cinebackup.txt"

#define SIZESTRING 50
#define SUCCESS 0
#define FAILURE -1
#define GETMAPPA 1
#define SOLDOUT -2
#define NOTPRESENT -11
#define GETBOOKING 12
#define DISDETTA 2
#define NOCOD -10
#define EXIT -3
#define SERVER_EXIT -4
#define POSTO_LIBERO 0
#define POSTO_OCCUPATO -1

#define BACKLOG 10
#define PORTNUMBER 54321


int serverInit();
int loadMap(int mappa[NFILE][NPOLTRONE], int *pt_postiliberi);

void manageSIGHUP(){
	puts("Hangup - terminale scollegato\n");
	exit(EXIT_SUCCESS);
}

void manageSIGINT(){
	puts("Interrupt\n");
	exit(EXIT_SUCCESS);
}

void manageSIGQUIT(){
	puts("Quit\n");
	exit(EXIT_SUCCESS);
}

void manageSIGILL(){
	fprintf(stderr, "Illegal instruction\n");
	exit(EXIT_FAILURE);
}

void manageSIGSEGV(){
	fprintf(stderr, "Segmentation violation\n");
	exit(EXIT_FAILURE);
}

void manageSIGTERM(){
	puts("Termination\n");
	exit(EXIT_SUCCESS);
}


int main() {

	struct sockaddr_in servaddr;
	int sizeservaddr = sizeof(servaddr);
	struct sockaddr_in clientaddr;
	int sizeclientaddr = sizeof(clientaddr);
	int listensock;
	int datasock;

	const int nfile = NFILE;
	const int npoltrone = NPOLTRONE;
	int mappa[NFILE][NPOLTRONE];
	int postiliberi = 0;
	int scelta;
	int codIO;
	int cod;
	int daprenotare;
	int listapoltrone[NFILE * NPOLTRONE];
	int i;
	int presente = -1;
	FILE *fileds;

	signal(SIGHUP, manageSIGHUP);
	signal(SIGINT, manageSIGINT);
	signal(SIGQUIT, manageSIGQUIT);
	signal(SIGILL, manageSIGILL);
	signal(SIGSEGV, manageSIGSEGV);
	signal(SIGTERM, manageSIGTERM);

	system("clear");
	scelta = serverInit();
	switch(scelta) {
		case SERVER_EXIT:
			exit(EXIT_SUCCESS);
			break;
		case FAILURE:
			system("clear");
			fprintf(stderr, "ATTENZIONE: Errore inizializzazione Server!\n");
			exit(EXIT_FAILURE);
			break;
		case SUCCESS:
			system("clear");
			puts("Inizializzazione avvenuta con successo!");
			break;
	}
	

	// connection setting
	bzero(&servaddr, sizeservaddr);
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORTNUMBER);
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	if((listensock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		fprintf(stderr, "Errore chimata socket()\n");
		exit(EXIT_FAILURE);
	}

	if((bind(listensock, (struct sockaddr *)&servaddr, sizeservaddr))<0){
		fprintf(stderr, "Errore chimata bind()\n");
		exit(EXIT_FAILURE);
	}

	if((listen(listensock, BACKLOG)) < 0){
		fprintf(stderr, "Errore chimata listen()\n");
		exit(EXIT_FAILURE);
	}

	puts("Server RUN...");
	puts("Premere <control> + <\\> quando si vuole terminare l'esecuzione");


	while(1){  // loop per continuare ad accettare connessioni

		bzero(&clientaddr, sizeclientaddr);
		if((datasock = accept(listensock, (struct sockaddr *)&clientaddr, (socklen_t *)&sizeclientaddr)) < 0){
			fprintf(stderr, "Errore chimata accept()\n");
			exit(EXIT_FAILURE);
		}

		// invia nome cinema
		if (write(datasock, NOMECINEMA, SIZESTRING) < 0) {
			fprintf(stderr, "Errore invio nome cinema al client\n");
			exit(EXIT_FAILURE);
		}

		// invia numero file
		if (write(datasock, &nfile, 4) < 0){
			fprintf(stderr, "Errore invio numero di file");
			exit(EXIT_FAILURE);
		}

		// invia numero poltrone
		if (write(datasock, &npoltrone, 4) < 0){
			fprintf(stderr, "Errore invio numero di poltrone");
			exit(EXIT_FAILURE);
		}


		do {  // finch� codIO != EXIT

			bzero(&codIO, 4);
			if (read(datasock, &codIO, 4) < 0) {
				fprintf(stderr, "Errore lettura codIO\n");
				exit(EXIT_FAILURE);
			}

			switch(codIO){

				case GETMAPPA:{

					// caricamento mappa cinema senza codici
					if (loadMap(mappa, &postiliberi) == FAILURE){
						codIO = FAILURE;
						if((write(datasock, &codIO, 4)) < 0){
							fprintf(stderr, "Errore nell'invio FAILURE\n");
							exit(EXIT_FAILURE);
						}
						fprintf(stderr, "Errore lettura mappa cinema dal file\n");
						exit(EXIT_FAILURE);
					}

					// rispondo SOLDOUT
					if (postiliberi == 0){
						codIO = SOLDOUT;
						if((write(datasock, &codIO, 4)) < 0){
							fprintf(stderr, "Errore invio SOLDOUT\n");
							exit(EXIT_FAILURE);
						}
						codIO = EXIT;
						break;

					} else {

						// invio numero di posti liberi
						codIO = postiliberi;  
						if((write(datasock, &codIO, 4)) < 0){
							fprintf(stderr, "Errore invio numero di posti liberi\n");
							exit(EXIT_FAILURE);
						}

						// invio mappa cinema
						if((write(datasock, mappa, sizeof(mappa))) < 0){  
							fprintf(stderr, "Errore invio mappa poltrone\n");
							exit(EXIT_FAILURE);
						}

					}
					break;
				}

				case GETBOOKING: {

					// generazione codice segreto
					srand(time(NULL));
					cod = rand()%100000;

					// lettura numero posti da prenotare
					bzero(&daprenotare, 4);
					if (read(datasock, &daprenotare, 4) < 0) {
						fprintf(stderr, "Errore lettura posti da prenotare\n");
						exit(EXIT_FAILURE);
					}

					// definizione booking
					int booking[daprenotare][2]; // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
					
					// lettura poltrone booking
					bzero(booking, sizeof(booking));
					if((read(datasock, booking, sizeof(booking)) ) < 0){ 
						fprintf(stderr, "Errore lettura poltrone da prenotare\n");
						exit(EXIT_FAILURE);
					}
					
					// si � gi� certi che i posti che il cliente vuole prenotare non sono occupati
					bzero(listapoltrone, sizeof(listapoltrone));
					// apertura file in lettura
					if((fileds = fopen(FILENAME, "r")) == NULL){
						codIO = FAILURE;
						if((write(datasock, &codIO, 4)) < 0){
							fprintf(stderr, "Errore invio FAILURE\n");
							exit(EXIT_FAILURE);
						}
						fprintf(stderr, "Errore lettura lista poltrone dal file\n");
						exit(EXIT_FAILURE);
					}

					// lettura codici da file e caricamento in array
					for(i = 0; i < NFILE * NPOLTRONE; i++){
						fscanf(fileds, "%d", &(listapoltrone[i]));
					}
					fclose(fileds);

					// inserisce il codice generato nelle posizioni delle poltrone prenotate
					for (i = 0; i < daprenotare; i++){
						listapoltrone[(NPOLTRONE * (booking[i][0])) + (booking[i][1])] = cod;  // (NPOLTRONE * numero fila) + numero poltrona
						printf("<%d> [%d - %d]\n", cod, booking[i][0] + 1, booking[i][1] + 1);
					}

					// apertura file in scrittura
					if ((fileds = fopen(FILENAME, "w")) == NULL ) {
						codIO = FAILURE;
						if((write(datasock, &codIO, 4)) < 0){
							fprintf(stderr, "Errore invio FAILURE\n");
							exit(EXIT_FAILURE);
						}
						fprintf(stderr, "Errore scrittura poltrone penotate sul file\n");
						exit(EXIT_FAILURE);
					}

					// riscrive tutto il file (codici vecchi e nuovi)
					for (i = 0; i < (NFILE * NPOLTRONE); i++){
						fprintf(fileds, "%d\n", listapoltrone[i]);
					}
					fclose(fileds);

					// invio codice segreto della prenotazione
					codIO = cod;
					if((write(datasock, &codIO, 4)) < 0){
						fprintf(stderr, "Errore invio codice segreto\n");
						exit(EXIT_FAILURE);
					}
					break;
				} // END CASE GETBOOKING

				case DISDETTA: {

					// apertura in lettura
					bzero(listapoltrone, sizeof(listapoltrone));
					if((fileds = fopen(FILENAME, "r")) == NULL){
						codIO = FAILURE;
						if((write(datasock, &codIO, 4)) < 0){
							fprintf(stderr, "Errore invio FAILURE\n");
							exit(EXIT_FAILURE);
						}
						fprintf(stderr, "Errore lettura lista poltrone dal file\n");
						exit(EXIT_FAILURE);
					}

					// carica contenuto del file nella listapoltrone
					for(i = 0; i < NFILE * NPOLTRONE; i++){
						fscanf(fileds, "%d", &(listapoltrone[i]));
					}
					fclose(fileds);

					// legge codice
					bzero(&codIO, 4);
					if (read(datasock, &codIO, 4) < 0) {
						fprintf(stderr, "Errore lettura codIO\n");
						exit(EXIT_FAILURE);
					}

					// controllo su listapoltrone
					// quando trova il codice lo sostituisce con 0
					for (i = 0; i < (NFILE * NPOLTRONE); i++){
						if(listapoltrone[i] == codIO){
							presente = 1;
							listapoltrone[i] = POSTO_LIBERO;
						}
					}

					// codice non presente nel file
					if (presente == -1){
						codIO = NOCOD;
						if((write(datasock, &codIO, 4)) < 0){
							fprintf(stderr, "Errore invio NOCOD\n");
							exit(EXIT_FAILURE);
						}
					}

					// codice presente nel file 
					if (presente == 1){
						if ((fileds = fopen(FILENAME, "w")) == NULL ) {
							codIO = FAILURE;
							if((write(datasock, &codIO, 4)) < 0){
								fprintf(stderr, "Errore invio FAILURE\n");
								exit(EXIT_FAILURE);
							}
							fprintf(stderr, "Errore aggiornamento file (disdetta)\n");
							exit(EXIT_FAILURE);
						}

						// riscrive tutta la lista (aggiornata) nel file
						for (i = 0; i < (NFILE * NPOLTRONE); i++){  
							fprintf(fileds, "%d\n", listapoltrone[i]);
						}
						fclose(fileds);

						codIO = SUCCESS; // conferma l'avvenuta disdetta
						if((write(datasock, &codIO, 4)) < 0){
							fprintf(stderr, "Errore invio SUCCESS\n");
							exit(EXIT_FAILURE);
						}
					}

					break;
					
				}  // END CASE DISDETTA

			} // END SWITCH

		} while(codIO != EXIT);

		close(datasock);
	}

	close(listensock);

	exit(EXIT_SUCCESS);
}



//--- FUNZIONI AUSILIARIE --------------------------------------------------------------------

int serverInit(){  // ritorna SUCCESS o FAILURE 

	FILE *fileds;
	char scelta;
	int i;

	system("clear");

	puts("------------------------------------------------------------");
	puts(" Riavviare il server oppure ripristino sessione precedente?");
	puts("------------------------------------------------------------");
	puts(" [1] Nuovo avvio");
	puts(" [2] Ripristino ultima sessione");
	puts(" [0] Exit");
	puts("------------------------------------------------------------");

	scanf(" %c", &scelta);

	switch(scelta){
		case '0':
			return SERVER_EXIT;
			break;

		case '1':
			if ((fileds = fopen(FILENAME, "w")) == NULL ) {
				fprintf(stderr, "Errore apertura file in scrittura\n");
				return FAILURE;
			}

			for (i = 0; i < (NFILE * NPOLTRONE); i++){
				fprintf(fileds, "%d\n", 0);
			}
			fclose(fileds);
			break;

		case '2':
			// controllo esistenza del file
			if((fileds = fopen(FILENAME, "r")) == NULL){
				fclose(fileds);
				system("clear");
				puts("ATTENZIONE: il file non esiste!");
				puts("Torno al menu principale...");
				sleep(2);
				serverInit();
			}
			break;

		default:
			system("clear");
			puts("ATTENZIONE - Inserimento errato!");
			puts("Torno alla pagina principale...");
			sleep(2);
			serverInit();
			break;
	}
	return SUCCESS;
}


// carica la mappa delle poltrone del cinema nella matrice mappa
// conta il numero di posti disponibili
// ritorna SUCCESS o FAILURE
int loadMap(int mappa[NFILE][NPOLTRONE], int *pt_postiliberi){

	FILE *fileds;
	int posto;
	(*pt_postiliberi) = 0;
	int x;
	int y;

	if((fileds = fopen(FILENAME, "r")) == NULL){
		fprintf(stderr, "Errore apertura file il lettura\n");
		return FAILURE;
	}

	for (x = 0; x < NFILE; x++){
		for (y = 0; y < NPOLTRONE; y++){
			fscanf(fileds, "%d", &posto);
			if (posto == 0){
				mappa[x][y] = POSTO_LIBERO;
				(*pt_postiliberi)++;
			} else {
				// per non inviare i codici al client
				mappa[x][y] = POSTO_OCCUPATO;
			}
		}
	}

	fclose(fileds);

	return SUCCESS;
}
